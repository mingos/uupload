module.exports = function(grunt) {
	// Load tasks
	require("load-grunt-tasks")(grunt);
	grunt.loadTasks("tasks");

	// Custom tasks
	grunt.registerTask("dev", ["concat:dev"]);
	grunt.registerTask("dist", ["concat:dev", "uglify:dist", "concat:dist"]);
	grunt.registerTask("default", ["dev"]);
	grunt.registerTask("test", ["jshint"]);
};
