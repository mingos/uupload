<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style>
		#drop {
			height: 100px;
			border: 3px dashed #e0e0e0;
			color: #c0c0c0;
			line-height: 100px;
			text-align: center;
			background-color: #f8f8f8;
			transition: all 150ms;
		}
		#drop.dragover {
			border-color: #c0e0ff;
			background-color: #f0f8ff;
		}
		.file {
			padding: 10px;
			border: 1px solid #e0e0e0;
			background-color: #f8f8f8;
			margin-bottom: 10px;
		}
		.file .filename {
			font-family: monospace;
		}
		.file .progress {
			height: 3px;
			background-color: white;
		}
		.file .progress .bar {
			height: 3px;
			background-color: #00a0ff;
			width: 0;
			-webkit-transition: width 100ms;
			        transition: width 100ms;
		}
		.file .progress .bar.done {
			background-color: #00c000;
		}
	</style>
	<script src="uupload.js"></script>
</head>
<body>
	<!-- the drop zone - drag and drop files here-->
	<div id="drop">drop files here</div>

	<!-- file input - it will pass the files directly to uUpload -->
	<input id="file" type="file">

	<!-- the notification area - information about uploaded files will be shown here -->
	<div id="info"></div>

	<script>
		// create an instance of the uUpload object
		var upload = new Mingos.uUpload("uupload.php");

		// enable the drop container
		var dropContainer = upload.createDropContainer(document.getElementById("drop"));

		// also enable the file input
		var fileInput = upload.createFileInput(document.getElementById("file"));

		// configure the decorator
		upload.setDecorator({
			elt: null,
			init: function() {
				// initialise the element
				this.elt = document.createElement("DIV");
				this.elt.classList.add("file");
				this.elt.innerHTML = '<div class="filename"></div><div class="progress"><div class="bar"></div></div>';
				this.elt.getElementsByClassName("filename")[0].textContent = this.getUploader().filename;

				// append the element to the parent container
				document.getElementById("info").appendChild(this.elt);

				// start upload immediately
				this.getUploader().startUpload();
			},
			success: function(response, status, statusText, headers) {
				this.elt.getElementsByClassName("bar")[0].style.width = "100%";
				this.elt.getElementsByClassName("filename")[0].textContent = this.getUploader().filename;
				this.elt.getElementsByClassName("bar")[0].classList.add("done");
			},
			progress: function(loaded, total)
			{
				this.elt.getElementsByClassName("bar")[0].style.width = (loaded / total * 100) + "%";
				this.elt.getElementsByClassName("filename")[0].textContent = this.getUploader().filename + " (" + loaded + " / " + total + ")";
			}
		});
	</script>
</body>
</html>
