window.Mingos = window.Mingos || {};

/**
 * The main uUpload object, controlling pretty much everything the library is able to do.
 * @param {String} targetURL The upload target URL
 * @constructor
 */
Mingos.uUpload = function(targetURL)
{
	"use strict";

	/**
	 * The form element name that will be communicated to the target script
	 * @type {String}
	 */
	var name = "file";

	/**
	 * The decorator to be instantiated with each new uploader object.
	 * @type {Object}
	 */
	var decorator = {};

	/**
	 * Set the file form element name
	 *
	 * @param  {String}         _name Form element name
	 * @return {Mingos.uUpload}       Provides a fluent interface
	 */
	this.setName = function(_name)
	{
		name = _name;

		return this;
	};

	/**
	 * Fetch the file form element's name
	 *
	 * @returns {String}
	 */
	this.getName = function()
	{
		return name;
	};

	/**
	 * Fetch the upload target URL
	 * @returns {String}
	 */
	this.getTargetURL = function()
	{
		return targetURL;
	};

	/**
	 * Set the decorator functions
	 *
	 * @param  {Object}         _decorator Decorator functions
	 * @return {Mingos.uUpload}            Provides a fluent interface
	 */
	this.setDecorator = function(_decorator)
	{
		decorator = _decorator;

		return this;
	};

	/**
	 * Create a new Uploader
	 * @param   {File}                    file File object to upload
	 * @returns {Mingos.uUpload.Uploader}
	 */
	this.createUploader = function(file)
	{
		var uploader = new Mingos.uUpload.Uploader(this, file);
		var _decorator = new Mingos.uUpload.Decorator(uploader);

		for (var i in decorator) {
			if (decorator.hasOwnProperty(i)) {
				_decorator[i] = decorator[i];
			}
		}

		uploader.setDecorator(_decorator);
		_decorator.init();

		return uploader;
	};
};

// add version number
Mingos.uUpload.VERSION = "<%= grunt.file.readJSON('package.json').version %>";
