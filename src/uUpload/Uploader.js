/**
 * An object handling file uploads via the HTML5 FormData object
 * @param {Mingos.uUpload} uUpload The parent uUpload object
 * @param {File}           file    The passed in file
 * @constructor
 */
Mingos.uUpload.Uploader = function(uUpload, file)
{
	"use strict";

	/**
	 * A FormData object used for storing the file data
	 * @type {FormData}
	 */
	var formData = new FormData();

	/**
	 * Decorator attached to the uploader
	 * @type {Mingos.uUpload.Decorator|undefined}
	 */
	var decorator;

	/**
	 * The name of the uploaded file
	 * @type {String}
	 */
	this.filename = file.name;

	// set the form data file to be sent
	formData.append(uUpload.getName(), file);

	/**
	 * Set a decorator for the uploader
	 *
	 * @param   {Mingos.uUpload.Decorator} _decorator A decorator that will handle the uploader's output
	 * @returns {Mingos.uUpload.Uploader}             Provides a fluent interface
	 */
	this.setDecorator = function(_decorator)
	{
		decorator = _decorator;

		return this;
	};

	/**
	 * Begin uploading the file
	 */
	this.startUpload = function()
	{
		var xhr = new XMLHttpRequest();

		// add a listener waiting for the upload to complete
		xhr.onreadystatechange = function() {
			if(xhr.readyState === 4) {
				// launch the complete callback
				decorator.complete();

				// launch the success/error callback
				if (xhr.status >= 200 && xhr.status < 400) {
					decorator.success(xhr.response, xhr.status, xhr.statusText, xhr.getAllResponseHeaders());
				} else {
					decorator.error(xhr.response, xhr.status, xhr.statusText, xhr.getAllResponseHeaders());
				}
			}
		};

		// also, add a listener that will forward the progress changes
		xhr.upload.addEventListener("progress", function(event) {
			decorator.progress(event.loaded, event.total);
		}, false);

		// send the request
		xhr.open("POST", uUpload.getTargetURL());
		xhr.send(formData);

		// launch the uploadStart callback
		decorator.uploadStart();
	};
};
