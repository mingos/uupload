/**
 * Direct interface between a form containing a file input and uUpload.
 * A FileList will be created and dispatched as soon as the form is sent. No other form data will be sent,
 * so it's best to use on forms containing nothing more than a single file input.
 *
 * @param {Mingos.uUpload}  uUpload  Parent uUpload object
 * @param {HTMLFormElement} element  Element to which the drop event should be attached
 * @constructor
 */
Mingos.uUpload.Form = function(uUpload, element)
{
	"use strict";

	if (!(element instanceof HTMLFormElement)) {
		throw "uUpload.Form expects to receive an HTMLFormElement.";
	}

	element.addEventListener("submit", function() {
		// find all file inputs
		var inputs = element.getElementsByTagName("INPUT");
		var files = [];
		var i, j, k, l;

		for (i = 0, j = inputs.length; i < j; ++i) {
			if (inputs[i].type === "file") {
				files.push(inputs[i].files);
			}
		}


		for (i = 0, j = files.length; i < j; ++i) {
			var file = files[i];
			for (k = 0, l = file.length; k < l; ++k) {
				uUpload.createUploader(file[k]);
			}
		}

		for (i = 0, j = inputs.length; i < j; ++i) {
			if (inputs[i].type === "file") {
				inputs[i].value = "";
			}
		}
	}, false);
};

// put the constructor in the uUpload object
Mingos.uUpload.prototype.createForm = function(_element) {
	return new Mingos.uUpload.Form(this, _element);
};
