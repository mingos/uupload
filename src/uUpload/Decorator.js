/**
 * Decorator object used to transform the output of an Uploader into a desired GUI feature.
 * This class is merely a base for all the derived decorators.
 * @param {Mingos.uUpload.Uploader} uploader The uploader object with which the decorator will communicate
 * @constructor
 */
Mingos.uUpload.Decorator = function(uploader)
{
	"use strict";

	/**
	 * Fetch the decorator's uploader
	 * @returns {Mingos.uUpload.Uploader}
	 */
	this.getUploader = function()
	{
		return uploader;
	};

	/**
	 * Function launched immediately after instantiation
	 */
	this.init = function() {};

	/**
	 * Function launched once as soon as the file upload starts
	 */
	this.uploadStart = function() {};

	/**
	 * Function launched when the progress indicator is updated
	 * @param {Number} loaded Bytes loaded so far
	 * @param {Number} total  Total size
	 */
	this.progress = function() {};

	/**
	 * Function launched when the file upload is complete
	 */
	this.complete = function() {};

	/**
	 * Function launched when the upload is complete, but resulted in an error.
	 * @param {*}      response
	 * @param {Number} status
	 * @param {String} statusText
	 * @param {String} headers
	 */
	this.error = function() {};

	/**
	 * Function launched when the file upload is complete and successful.
	 * @param {*}      response
	 * @param {Number} status
	 * @param {String} statusText
	 * @param {String} headers
	 */
	this.success = function() {};
};
