/**
 * Drop container.
 * Upon passing in a DOM object, that DOM object will start listening to the drop event.
 * The drop container will communicate with uUpload and ask it to create new uploaders with the
 * files received.
 * Also, the container element will have the "dragover" class set when something is dragged over it.
 *
 * @param {Mingos.uUpload} uUpload  Parent uUpload object
 * @param {HTMLElement}    element  Element to which the drop event should be attached
 * @constructor
 */
Mingos.uUpload.DropContainer = function(uUpload, element)
{
	"use strict";

	if (!(element instanceof HTMLElement)) {
		throw "uUpload.DropContainer expects to receive a HTMLElement.";
	}

	element.addEventListener("dragenter", function() {
		element.classList.add("dragover");
	}, false);

	element.addEventListener("dragleave", function() {
		element.classList.remove("dragover");
	}, false);

	element.addEventListener("dragover", function(event) {
		event.stopPropagation();
		event.preventDefault();
		event.dataTransfer.dropEffect = "copy";
	}, false);

	element.addEventListener("drop", function(event) {
		event.stopPropagation();
		event.preventDefault();

		element.classList.remove("dragover");

		var files = event.dataTransfer.files;

		for (var i = 0, j = files.length; i < j; ++i) {
			uUpload.createUploader(files[i]);
		}
	}, false);
};

// put the constructor in the uUpload object
Mingos.uUpload.prototype.createDropContainer = function(_element) {
	return new Mingos.uUpload.DropContainer(this, _element);
};
