/**
 * Direct interface between a file input and uUpload.
 * Files added to the fileInput will be passed directly to uUpload, and the input's FileList will
 * be cleared.
 *
 * @param {Mingos.uUpload}   uUpload  Parent uUpload object
 * @param {HTMLInputElement} element  Element to which the drop event should be attached
 * @constructor
 */
Mingos.uUpload.FileInput = function(uUpload, element)
{
	"use strict";

	if (!(element instanceof HTMLInputElement)) {
		throw "uUpload.FileInput expects to receive an HTMLInputElement.";
	}

	element.addEventListener("change", function() {
		var files = element.files;

		for (var i = 0, j = files.length; i < j; ++i) {
			uUpload.createUploader(files[i]);
		}

		element.value = "";
	}, false);
};

// put the constructor in the uUpload object
Mingos.uUpload.prototype.createFileInput = function(_element) {
	return new Mingos.uUpload.FileInput(this, _element);
};
