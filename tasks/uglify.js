module.exports = function(grunt) {
	grunt.config.set("uglify", {
		dist: {
			options: {
				mangle: true,
				compress: {
					drop_console: true
				}
			},
			files: {
				"dist/uupload.min.js": [
					"dist/uupload.js"
				]
			}
		}
	});
};
