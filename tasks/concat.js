module.exports = function(grunt) {
	grunt.config.set("concat", {
		dev: {
			options: {
				process: function(src) {
					var ret = src
						.replace(/\s*['"]use strict['"];.*\n/g, "")
						.replace(/<%=([^%>]*)%>/g, function(str, match) {
							return eval(match);
						});
					return ret;
				},
				banner: "/*! uUpload <%= grunt.file.readJSON('package.json').version %>\n"
					+ "    Licence: BSD 3-Clause */\n"
					+ "(function(window) {\n"
					+ "\"use strict\";\n\n",
				footer: "})(window);\n"
			},
			files: {
				"dist/uupload.js": [
					"src/**/*.js"
				]
			}
		},
		dist: {
			options: {
				banner: "/*! uUpload <%= grunt.file.readJSON('package.json').version %>\n"
					+ "    Licence: BSD 3-Clause */\n"
			},
			files: {
				"dist/uupload.min.js": [
					"dist/uupload.min.js"
				]
			}
		}
	});
};
