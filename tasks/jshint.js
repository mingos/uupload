module.exports = function(grunt) {
	grunt.config.set("jshint", {
		test: {
			options: {
				browser: true,
				camelcase: true,
				curly: true,
				eqeqeq: true,
				es3: true,
				forin: true,
				freeze: true,
				immed: true,
				indent: 4,
				latedef: true,
				maxlen: 120,
				newcap: true,
				noarg: true,
				noempty: true,
				nomen: true,
				nonbsp: false,
				nonew: true,
				plusplus: false,
				quotmark: true,
				undef: true,
				strict: false,
				trailing: true,
				unused: true,
				globals: {
					Mingos: true
				}
			},
			files: {
				src: ["src/**/*.js"]
			}
		}
	});
};
